# -*- coding: utf-8 -*-

import logging
import os, sys
import pprint, json, pickle, yaml, operator
from subprocess import (run, check_call)

from pathlib import Path

import yaml

from utils.extra import (bootstrap_kolla, generate_inventory, get_vip_pool,
                         pop_ip, in_kolla, seekpath)
from utils.constants import (ANSIBLE_DIR, INVENTORY_DIR, MAD_RESULT_DIR_NAME)

from enoslib.task import enostask
from enoslib.api import discover_networks, play_on, run_command, run_ansible
from utils.extra import run_ansible as local_run_ansible

from utils.analysis import (format_results, generate_theoretic_dots,
        extract_measured_times, compute_theoretic_times,
        compute_means_stds_maxs, draw_deployment_charts, generate_latex_table)

from enoslib.infra.enos_vagrant.provider import Enos_vagrant
from enoslib.infra.enos_g5k.provider import G5k
from enoslib.infra.enos_vmong5k.provider import VMonG5k
from enoslib.infra.enos_vagrant.configuration import \
    Configuration as VagrantConfiguration
from enoslib.infra.enos_g5k.configuration import \
    Configuration as G5kConfiguration, \
    NetworkConfiguration as G5kNetworkConfiguration
from enoslib.infra.enos_vmong5k.configuration import \
    Configuration as VMonConfiguration



@enostask(new=True)
def deploy(bootstrap, config_file, force_deploy, provider_type, restore, tags, env=None):
    # Extract the configuration from the given file and copy in the environment
    configuration = load_config(config_file)
    env['config'].update(configuration)
    env['config_file'] = config_file

    # Create the provider config and get the related resources from the provider
    provider = None
    config = None
    if provider_type == 'g5k' and 'g5k' in env['config']:
        env['provider'] = 'g5k'
        config = G5kConfiguration.from_dictionnary(env['config']['g5k'])
        provider = G5k(config)
    elif provider_type == 'vagrant' and 'vagrant' in env['config']:
        env['provider'] = 'vagrant'
        config = VagrantConfiguration.from_dictionnary(env['config']['vagrant'])
        provider = Enos_vagrant(config)
    else:
        print("Provider type is not correct")
        sys.exit(1)

    # Save the provider in the environment (re-used when destroy is called)
    env['config']['provider'] = provider

    # Fetch roles and networks and copy in the environment
    roles, networks = get_roles_and_networks(provider, force_deploy)
    env['rsc'] = roles
    env['networks'] = networks

    # Generate the inventory that will be used by ansible by merging information
    # from the base_inventory and information related to the provider resources
    inventory = os.path.join(env['resultdir'], 'multinode')
    env['inventory'] = inventory
    inventory_conf = None
    if not inventory_conf:
        logging.debug("No inventory specified, using the sample.")
        base_inventory = os.path.join(INVENTORY_DIR, 'inventory.sample')
    else:
        base_inventory = seekpath(inventory_conf)
    generate_inventory(env['rsc'], env['networks'], base_inventory, inventory)
    logging.info('Generates inventory %s' % inventory)

    # Add some variables required by ansible playbooks in the environment
    vip_pool = get_vip_pool(networks)
    env['config'].update({
       'vip':               pop_ip(vip_pool),
       'registry_vip':      pop_ip(vip_pool),
       'influx_vip':        pop_ip(vip_pool),
       'grafana_vip':       pop_ip(vip_pool),
       'resultdir':         env['resultdir'],
       'rabbitmq_password': "demo",
       'database_password': "demo"
    })

    # Get kolla-ansible and install its dependencies
    get_and_bootstrap_kolla(env)

    # If the benchmark is supposed to be run on a dedicated node on the testbed
    # (concerto-node), this step prepares the files that will be transferred by
    # ansible
    if bootstrap:
        cmd = ["mkdir ansible/roles/enos_common/tasks/files"]
        run(cmd, shell=True)

        # TODO To be removed once we get a stable dev branch
        # Prepare to restore the previous results if needed
        if restore:
            backup_dir = os.path.abspath(restore)
            backup_dir_mad = os.path.join(backup_dir, 'mad.tar.gz')
            backup_target = os.path.abspath("ansible/roles/enos_common/tasks/files/")

            cmd = ["tar -C " + backup_target + " -xvzf "
                    + str(backup_dir_mad) + " mad/" + MAD_RESULT_DIR_NAME]
            run(cmd, shell=True)

        cmd = ["make -Bs package &&" + \
                " mv mad.tgz ansible/roles/enos_common/tasks/files/"]
        run(cmd, shell=True)

        cmd = ["cp -P current ansible/roles/enos_common/tasks/files/"]
        run(cmd, shell=True)

        cmd = ["cp " + env['config_file'] + \
                " ansible/roles/enos_common/tasks/files/reservation.yaml"]
        run(cmd, shell=True)

    # Set global parameters for the playbook (action + context)
    options = {}
    options.update(env['config'])
    enos_action = "deploy"
    options.update(enos_action=enos_action)
    options.update(context=str(provider_type))
    if restore:
        options.update(restore=backup_target)

    # Call the enos.yml playbook with required parameters
    playbook = os.path.join(ANSIBLE_DIR, 'enos.yml')
    run_ansible([playbook],
            inventory_path = env['inventory'],
            roles = roles,
            extra_vars = options,
            tags = tags)


@enostask()
def redeploy(config_file, force_deploy, provider_type, registry, tags, env=None):
    # TODO: not very clean, some variables in the environment generated on local
    # machine are absolute paths which does fit on concerto-node. The following
    # workaround replaces those variables on-the-fly
    env['resultdir'] = str(Path('.') / 'current')
    env['cwd'] = str(Path('.'))
    env['inventory'] = str(Path('.') / 'current' / 'multinode')

    # Can't serialize because of the provider class, workaround:
    env['config']['provider']  = None

    # An environment must be fetched by enostask(), otherwise ragequit
    if not env:
        sys.exit(1)

    # Adapt the registry configuration
    if not registry:
        env['config']['registry'] = {'type': 'none'}

    options = {}
    options.update(env['config'])
    playbook = os.path.join(ANSIBLE_DIR, 'enos.yml')
    local_run_ansible(playbook=playbook,
            inventory=env['inventory'],
            config_vars=['enos_action=deploy', 'context=' + str(provider_type),
                json.dumps(env['config'])],
            tags = tags)


@enostask()
def install_os(pull, reconfigure, tags, env, **kwargs):
    """Deploy OpenStack with kolla-ansible."""
    logging.debug('phase[os]: args=%s' % kwargs)

    kolla_path = get_and_bootstrap_kolla(env, force=True)
    # Construct kolla-ansible command...
    kolla_cmd = [os.path.join(kolla_path, "tools", "kolla-ansible")]

    if reconfigure:
        kolla_cmd.append('reconfigure')
    elif pull:
        kolla_cmd.append('pull')
    else:
        kolla_cmd.append('deploy')

    kolla_cmd.extend(["-i", "%s/multinode" % env['resultdir'],
                      "--passwords", "%s/passwords.yml" % env['resultdir'],
                      "--configdir", "%s" % env['resultdir']])

    if tags:
        kolla_cmd.extend(['--tags', tags])

    logging.info("Calling Kolla...")

    in_kolla(kolla_cmd)


@enostask()
def backup(backup_dir, env=None, **kwargs):
    backup_dir = os.path.abspath(backup_dir)
    # create if necessary
    if not os.path.isdir(backup_dir):
        os.makedirs(backup_dir + "/logs")
        os.makedirs(backup_dir + "/confs")
        os.makedirs(backup_dir + "/results")
    # update the env
    # TODO write backup_dir in env
    env['config']['backup_dir'] = backup_dir
    options = {}
    options.update(env['config'])
    options.update({'enos_action': 'backup'})
    playbook_path = os.path.join(ANSIBLE_DIR, 'enos.yml')
    inventory_path = os.path.join(env['resultdir'], 'multinode')
    run_ansible([playbook_path], inventory_path, extra_vars=options)


@enostask()
def analyze(backup_dir, exclude=[], env=None, **kwargs):
    from assemblies.assembly_seq_1t.assembly import Seq1tAssembly
    from assemblies.assembly_dag_2t.assembly import Dag2tAssembly
    from assemblies.assembly_dag_nt4.assembly import DagNT4Assembly
    from assemblies.assembly_seq_nt4.assembly import SeqNT4Assembly

    logging.getLogger('matplotlib').setLevel(logging.ERROR)
    logging.getLogger('urllib3').setLevel(logging.ERROR)

    backup_dir = os.path.abspath(backup_dir)
    results_dir = os.path.abspath(backup_dir + "/results/")
    charts_dir = os.path.abspath(backup_dir + "/charts/")
    if not os.path.isdir(charts_dir):
        os.makedirs(charts_dir)
    # TODO if no result_dir, get it from env or exit

    # Get the assembly deployment time results
    result_file = os.path.abspath(results_dir + "/results_deployment_times")
    with open(result_file, "r") as f:
        results = yaml.load(f)

    # Format the previous results and detect assemblies, registry modes and the
    # number of iterations:
    formatted_results, assemblies, \
        registries, num_iterations = format_results(results)

    # Adapt the list of assemblies to user's parameters
    if exclude:
        for assembly in exclude:
            assemblies = [ x for x in assemblies if x != assembly ]

    # Generate Gantt charts from transition times contained in JSON files:
    experiment_reconfigurations = {
        "seq_1t": {
            "reconf": Seq1tAssembly.get_deploy_reconf(),
            "time_files_pattern": "seq_1t"
        },
        "dag_2t": {
            "reconf": Dag2tAssembly.get_deploy_reconf(),
            "time_files_pattern": "dag_2t"
        },
        "seq_nt4": {
            "reconf": SeqNT4Assembly.get_deploy_reconf(),
            "time_files_pattern": "seq_nt4"
        },
        "dag_nt4": {
            "reconf": DagNT4Assembly.get_deploy_reconf(),
            "time_files_pattern": "dag_nt4"
        },
        "dag_t_seq_nt4": {
            "reconf": DagNT4Assembly.get_deploy_reconf(),
            "time_files_pattern": "seq_nt4"
        }
    }

    # Generate formula
    formula = generate_theoretic_dots(assemblies, experiment_reconfigurations,
            charts_dir)

    # Extract measure times
    measured_times = extract_measured_times(assemblies, registries, \
            num_iterations, experiment_reconfigurations, results_dir)

    # Compute required information about theoretical results
    theoretic_times = compute_theoretic_times(assemblies, registries,
            experiment_reconfigurations, measured_times, charts_dir)

    # Compute required information about measured results
    means, stds, maxs = compute_means_stds_maxs(assemblies, registries, formatted_results)

    # Generate benchmark chart
    table_results, plot = draw_deployment_charts(formatted_results, theoretic_times,
            assemblies, registries, means, stds, maxs)
    plot.savefig(charts_dir + "/results_deployment_times.svg")

    # Generate the LaTeX table
    renderer_template = generate_latex_table(assemblies, registries, table_results)
    with open("%s/tab_openstack_results.tex" % charts_dir, "w") as f:
        f.write(renderer_template)


@enostask()
def bench(conf, test, provider, env=None, **kwargs):
    options = {}
    options.update(env['config'])
    options.update(conf=conf)
    options.update(test=test)
    options.update(provider=provider)

    with play_on(pattern_hosts="concerto-node", roles=env['rsc']) as p:
        p.apt(
                name=["tmux"],
                state="present")
        p.synchronize(
                src="current",
                dest="/mad/",
                copy_links="yes")
        p.shell("tmux new-session" \
                + " -d 'cd /mad; python3 bench.py " \
                + " -p " + provider \
                + " -t " + test + " -L -M -c " +  MAD_RESULT_DIR_NAME + "'")


@enostask()
def info(out, env):
    if not out:
        pprint.pprint(env)
    elif out == 'json':
        print(json.dumps(env, default=operator.attrgetter('__dict__')))
    elif out == 'pickle':
        print(pickle.dumps(env))
    elif out == 'yaml':
        print(yaml.dump(env))
    else:
        print("--out doesn't suppport %s output format" % out)
        print(info.__doc__)


@enostask()
def destroy(hard, include_images, local, env, **kwargs):
    # Same workaround as in redeploy()
    env['resultdir'] = str(Path('.') / 'current')
    env['cwd'] = str(Path('.'))
    env['inventory'] = str(Path('.') / 'current' / 'multinode')

    if hard:
        logging.info('Destroying all the resources')
        provider = env['config']['provider']
        provider.destroy()
    elif local:
        logging.info('Remove kolla images and containers on OpenStack nodes')
        playbook_path = os.path.join(ANSIBLE_DIR, 'clean_hosts.yml')
        inventory_path = os.path.join(env['resultdir'], 'multinode')
        run_ansible([playbook_path], inventory_path, extra_vars=env['config'])
    else:
        command = ['destroy', '--yes-i-really-really-mean-it']
        if include_images:
            logging.info('Remove kolla images and containers on every nodes')
            command.append('--include-images')
        kolla_kwargs = {'--': True,
                #'--env': env,
                #'-v': verbosity,
                '<command>': command,
                #'--silent': silent,
                'kolla': True}
        options = {
            "enos_action": "destroy"
        }
        up_playbook = os.path.join(ANSIBLE_DIR, 'enos.yml')

        inventory_path = os.path.join(env['resultdir'], 'multinode')
        # Destroying enos resources
        run_ansible([up_playbook], inventory_path, extra_vars=options)
        # Destroying kolla resources
        _kolla(env=env, **kolla_kwargs)


@enostask()
def bootstrap(cached, env, **kwargs):
    logging.debug('phase[bootstrap]: args=%s' % kwargs)

    env['resultdir'] = str(Path('.') / 'current')
    env['cwd'] = str(Path('.'))
    env['inventory'] = str(Path('.') / 'current' / 'multinode')

    # Runs playbook that initializes resources (eg,
    # installs the registry, install monitoring tools, ...)
    bootstrap_playbook = os.path.join(ANSIBLE_DIR, 'bootstrap.yml')
    if cached:
        bootstrap_playbook = os.path.join(ANSIBLE_DIR, 'bootstrap_reg_cached.yml')
    run_ansible([bootstrap_playbook], env['inventory'],
            extra_vars=env['config'])


def load_config(file_path):
    """
    Read configuration from a file in YAML format.
    :param file_path: Path of the configuration file.
    :return:
    """
    with open(file_path) as f:
        configuration = yaml.safe_load(f)
    return configuration


def vmon_gr5k_provider():
    logging.basicConfig(level=logging.DEBUG)
    # Define the resources we wanted, this is provider specific
    conf = VMonConfiguration.from_settings(job_name="concerto-openstack",
                                           image="/grid5000/virt-images/debian9-x64-std-2019040916.qcow2",
                                           gateway=True) \
        .add_machine(roles=["concerto-node"],
                     cluster=cluster,
                     number=1) \
        .add_machine(roles=["openstack", "control"],
                     cluster=cluster,
                     number=control_nodes) \
        .add_machine(roles=["openstack", "compute"],
                     cluster=cluster,
                     number=compute_nodes) \
        .add_machine(roles=["openstack", "network"],
                     cluster=cluster,
                     number=network_nodes) \
        .finalize()

    return VMonG5k(conf)


def get_roles_and_networks(provider, force_deploy):
    """Start the resources and returns roles and networks."""
    logging.basicConfig(level=logging.DEBUG)

    # Start the resources
    roles, networks = provider.init(force_deploy=force_deploy)

    # Grab information from the returned roles
    discover_networks(roles, networks)

    return roles, networks


def _kolla(env=None, **kwargs):
    """Forge the command sent to kolla-ansible."""
    logging.info('Kolla command')
    logging.info(kwargs)
    kolla_path = get_and_bootstrap_kolla(env, force=False)
    kolla_cmd = [os.path.join(kolla_path, "tools", "kolla-ansible")]
    kolla_cmd.extend(kwargs['<command>'])
    kolla_cmd.extend(["-i", "%s/multinode" % env['resultdir'],
                      "--passwords", "%s/passwords.yml" % env['resultdir'],
                      "--configdir", "%s" % env['resultdir']])
    logging.info(kolla_cmd)
    in_kolla(kolla_cmd)


def get_and_bootstrap_kolla(env, force=False):
    """This gets kolla in the current directory.

    force if a potential previous installation must be overwritten.
    """

    kolla_path = Path('.') / "current" / "kolla"

    if force and os.path.isdir(kolla_path):
        logging.info("Remove previous Kolla installation")
        check_call("rm -rf %s" % kolla_path, shell=True)
    if not os.path.isdir(kolla_path):
        logging.info("Cloning Kolla repository...")
        check_call("git clone %s --branch %s --single-branch --quiet %s" %
                       (env['config']['kolla_repo'],
                        env['config']['kolla_ref'],
                        kolla_path),
                   shell=True)

        # Bootstrap kolla running by patching kolla sources (if any) and
        # generating admin-openrc, globals.yml, passwords.yml
        bootstrap_kolla(env)

        # Installing the kolla dependencies in the kolla venv
        in_kolla('cd %s && pip install .' % kolla_path)
        # Kolla recommends installing ansible manually.
        # Currently anything over 2.3.0 is supported, not sure about the future
        # So we hardcode the version to something reasonnable for now
        in_kolla('pip install ansible==2.5.7')

        # Installation influxdb client, used by the annotations
        enable_monitoring = env['config'].get('enable_monitoring')
        if enable_monitoring:
            in_kolla('pip install influxdb')

    return kolla_path


def add_registry(env, provider):
    """Add a registry node in the configuration, based on control-node."""
    # Grab the definition of control-node
    control_def = None
    machines = env['config'][provider]['resources']['machines']
    for machine in machines:
        if "control" in machine['roles']:
            control_def = machine

    # Create registry-node from control-node
    registry_def = control_def.copy()
    registry_def['roles'] = ['disco/registry']

    # Update the type of the registry in the configuration (used by mad deploy)
    env['registry'] = {'type': 'internal'}

    machines.append(registry_def)


def del_registry(env, provider):
    """Delete any extra node from the configuration."""
    # Delete any definition of registry nodes in the configuration
    machines = env['config'][provider]['resources']['machines']
    for machine in machines:
        if "disco/registry" in machine['roles']:
            machines.remove(machine)

    # Update the type of the registry in the configuration (used by mad deploy)
    env['registry'] = {'type': 'none'}

