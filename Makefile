
PACKAGE_TARGETS := ansible ansible.cfg \
	assemblies assembly.py \
	bench.py inventories mad.py Makefile rabbit_vars.yml \
	README.md requirements.txt reservation* tasks.py utils

CTAGS := $(shell command -v ctags 2> /dev/null)
CTAGS-exists : ; @which ctags > /dev/null
PIP-exists := $(shell command -v pip3 2> /dev/null)

check_pip3:
ifndef PIP-exists
	$(error "pip3 is not available - please install it.")
endif

install_deps: check_pip3 requirements.txt
	pip3 install -r requirements.txt

remove_deps:
	-rm -rf venv

package: mad.tgz
mad.tgz: $(PACKAGE_TARGETS)
	tar -zhcvf $@\
		--exclude "current"\
		--exclude ".vagrant"\
		--exclude "enos_20*"\
		--exclude "venv"\
		--exclude "__pycache__"\
		--exclude "*.swo"\
		--exclude "*.swp" --transform 's,^,mad/,' $^

tags: CTAGS-exists
	ctags -R --fields=+l --languages=python --python-kinds=-iv \
	    --exclude=backups --exclude=ansible --exclude=expes

clean:
	-rm -rf enos_*
	-rm -rf MadBench*
	-rm -rf results*.json

mrproper: clean
	-rm -rf current
