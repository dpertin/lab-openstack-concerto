
import logging
from pathlib import Path

from concerto.component import Component
from concerto.dependency import DepType

from utils.extra import run_ansible
from utils.extra import get_host_name_and_ip
from utils.constants import (ANSIBLE_DIR, SYMLINK_NAME)



class Glance(Component):

    def __init__(self):
        self.playbook = str(Path(ANSIBLE_DIR) / "site_glance.yml")
        self.inventory = str(Path(SYMLINK_NAME) / "multinode")

        Component.__init__(self)

    def create(self):
        self.places = [
            'initiated',
            'pulled',
            'deployed'
        ]

        self.transitions = {
            'pull': ('initiated', 'pulled', 'deploy', 0, self.pull),
            'config': ('initiated', 'pulled', 'deploy', 0, self.config),
            'register': ('initiated', 'pulled', 'deploy', 0, self.register),
            'deploy': ('pulled', 'deployed', 'deploy', 0, self.deploy)
        }

        self.dependencies = {
            'mariadb': (DepType.USE, ['config']),
            'keystone': (DepType.USE, ['register']),
            'glance': (DepType.PROVIDE, ['deployed']),
            #'mdbd': (DepType.DATA_USE, ['config']),
            #'kstd': (DepType.DATA_USE, ['config', 'register']),
            #'rabd': (DepType.DATA_USE, ['config']),
            #'glad': (DepType.DATA_PROVIDE, ['initiated'])
        }

        self.initial_place = "initiated"

    def pull(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Pulling Glance component", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=pull"])

    def config(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Configuring Glance component", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_config"])

    def register(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Registering Glance component", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_register"])

    def deploy(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Deploying Glance component", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_deploy"])

