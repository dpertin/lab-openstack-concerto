
import logging
from pathlib import Path

from concerto.component import Component
from concerto.dependency import DepType

from utils.extra import run_ansible
from utils.extra import get_host_name_and_ip
from utils.constants import (ANSIBLE_DIR, SYMLINK_NAME)



class MariaDB(Component):

    def __init__(self):
        self.playbook = str(Path(ANSIBLE_DIR) / "site_mariadb.yml")
        self.inventory = str(Path(SYMLINK_NAME) / "multinode")

        Component.__init__(self)

    def create(self):
        self.places = [
            'initiated',
            'configured',
            'bootstrapped',
            'restarted',
            'registered',
            'deployed'
        ]

        self.transitions = {
            'pull': ('initiated', 'configured', 'deploy', 0, self.pull),
            'config': ('initiated', 'configured', 'deploy', 0, self.config),
            'bootstrap': ('configured', 'bootstrapped', 'deploy', 0, self.bootstrap),
            'restart': ('bootstrapped', 'restarted', 'deploy', 0, self.restart),
            'register': ('restarted', 'registered', 'deploy', 0, self.register),
            'check': ('registered', 'deployed', 'deploy', 0, self.check)
        }

        self.dependencies = {
            'common': (DepType.USE, ['register']),
            'haproxy': (DepType.USE, ['restart']),
            'mariadb': (DepType.PROVIDE, ['deployed'])
        }

        self.initial_place = 'initiated'

    def pull(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Pulling mariadb start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=pull"])

    def config(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Configurating mariadb start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_config"])

    def bootstrap(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Bootstrapping mariadb start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_bootstrap"])

    def restart(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Restarting mariadb start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_restart"])

    def register(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Registering mariadb start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_register"])

    def check(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Checking mariadb start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_check"])

