
import logging
from pathlib import Path

from concerto.component import Component
from concerto.dependency import DepType

from utils.extra import run_ansible
from utils.extra import get_host_name_and_ip
from utils.constants import (ANSIBLE_DIR, SYMLINK_NAME)



class Keystone(Component):

    def __init__(self):
        self.playbook = str(Path(ANSIBLE_DIR) / "site_keystone.yml")
        self.inventory = str(Path(SYMLINK_NAME) / "multinode")

        Component.__init__(self)

    def create(self):
        self.places = [
            'initiated',
            'pulled',
            'deployed'
        ]

        self.transitions = {
            'pull': ('initiated', 'pulled', 'deploy', 0, self.pull),
            'deploy': ('pulled', 'deployed', 'deploy', 0, self.deploy)
        }

        self.dependencies = {
            'mariadb': (DepType.USE, ['deploy']),
            'keystone': (DepType.PROVIDE, ['deployed'])
            #'mdbd': (DepType.DATA_USE, ['deploy']),
            #'kstd': (DepType.DATA_PROVIDE, ['initiated']),
        }

        self.initial_place = 'initiated'

    def pull(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Pulling keystone start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=pull"])

    def deploy(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Deploying keystone start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=deploy"])

