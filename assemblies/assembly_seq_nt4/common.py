
import logging
from pathlib import Path

from concerto.component import Component
from concerto.dependency import DepType

from utils.extra import run_ansible
from utils.extra import get_host_name_and_ip
from utils.constants import (ANSIBLE_DIR, SYMLINK_NAME)



class Common(Component):

    def __init__(self):
        self.playbook = str(Path(ANSIBLE_DIR) / "site_common.yml")
        self.inventory = str(Path(SYMLINK_NAME) / "multinode")

        Component.__init__(self)

    def create(self):

        self.places = [
            'initiated',
            'deployed',
            'ktb_deployed'
        ]

        self.transitions = {
            'ktb_deploy': ('initiated', 'ktb_deployed', 'deploy', 0, self.ktb_deploy),
            'deploy': ('ktb_deployed', 'deployed', 'deploy', 0, self.deploy)
        }

        self.dependencies = {
            'common': (DepType.PROVIDE, ['deployed']),
            'facts': (DepType.USE, ['ktb_deploy'])
        }

        self.initial_place = 'initiated'

    def deploy(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : oth deployment of common start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=oth_deploy"])

    def ktb_deploy(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : ktb deployment of common start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=ktb_deploy"])

