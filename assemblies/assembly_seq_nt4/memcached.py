
import logging
from pathlib import Path

from concerto.component import Component
from concerto.dependency import DepType

from utils.extra import run_ansible
from utils.extra import get_host_name_and_ip
from utils.constants import (ANSIBLE_DIR, SYMLINK_NAME)



class MemCached(Component):
    """ Defines a component for the deployment of Memcached """

    def __init__(self):
        self.playbook = str(Path(ANSIBLE_DIR) / "site_memcached.yml")
        self.inventory = str(Path(SYMLINK_NAME) / "multinode")

        Component.__init__(self)

    def create(self):

        self.places = [
            'initiated',
            'deployed'
        ]

        self.transitions = {
            'deploy': ('initiated', 'deployed', 'deploy', 0, self.deploy)
        }

        self.dependencies = {
            'haproxy': (DepType.USE, ['deploy']),
            'memcached': (DepType.PROVIDE, ['deployed'])
        }

        self.initial_place = 'initiated'

    def deploy(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Deploying memcached start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=deploy"])

