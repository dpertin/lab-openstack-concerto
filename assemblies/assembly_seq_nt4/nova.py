
import logging
from pathlib import Path

from concerto.component import Component
from concerto.dependency import DepType

from utils.extra import run_ansible
from utils.extra import get_host_name_and_ip
from utils.constants import (ANSIBLE_DIR, SYMLINK_NAME)



class Nova(Component):

    def __init__(self):
        self.playbook = str(Path(ANSIBLE_DIR) / "site_nova.yml")
        self.inventory = str(Path(SYMLINK_NAME) / "multinode")

        Component.__init__(self)

    def create(self):
        self.places = [
            'initiated',
            'pulled',
            'configured',
            'registered',
            'created',
            'upgraded',
            'upgraded_api',
            'restarted',
            'deployed'
        ]

        self.transitions = {
            'pull': ('initiated', 'pulled', 'deploy', 0, self.pull),
            'config': ('pulled', 'configured', 'deploy', 0, self.config),
            'register': ('configured', 'registered', 'deploy', 0, self.register),
            'create_db': ('registered', 'created', 'deploy', 0, self.create_db),
            'upgrade_db': ('created', 'upgraded', 'deploy', 0, self.upgrade_db),
            'upgrade_api_db': ('upgraded', 'upgraded_api', 'deploy', 0, self.upgrade_api_db),
            'restart': ('upgraded_api', 'restarted', 'deploy', 0, self.restart),
            'cell_setup': ('restarted', 'deployed', 'deploy', 0, self.cell_setup),
        }

        self.dependencies = {
            'glance': (DepType.USE, ['pull']),
            'nova': (DepType.PROVIDE, ['deployed'])
        }

        self.initial_place = 'initiated'

    def pull(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Pulling Nova start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=pull"])

    def config(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Configuring Nova start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_config"])

    def create_db(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Creating Database Nova start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_create_db"])

    def register(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Registering Nova start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_register"])

    def upgrade_db(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Upgrading database Nova start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_upgrade_db"])

    def upgrade_api_db(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Upgrading API db Nova start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_upgrade_api_db"])

    def restart(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Restarting Nova start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_restart"])

    def cell_setup(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Deploying Nova start", host, ip)
        return run_ansible(self.playbook, self.inventory,
                ["action=mad_simple_cell_setup"])

