
import logging
from pathlib import Path

from concerto.component import Component
from concerto.dependency import DepType

from utils.extra import run_ansible
from utils.extra import get_host_name_and_ip
from utils.constants import (ANSIBLE_DIR, SYMLINK_NAME)



class Neutron(Component):

    def __init__(self):
        self.playbook = str(Path(ANSIBLE_DIR) / "site_neutron.yml")
        self.inventory = str(Path(SYMLINK_NAME) / "multinode")

        Component.__init__(self)

    def create(self):
        self.places = [
            'initiated',
            'pulled',
            'configured',
            'registered',
            'deployed'
        ]

        self.transitions = {
            'pull': ('initiated', 'pulled', 'deploy', 0, self.pull),
            'config': ('pulled', 'configured', 'deploy', 0, self.config),
            'register': ('configured', 'registered', 'deploy', 0, self.register),
            'deploy': ('registered', 'deployed', 'deploy', 0, self.deploy)
        }

        self.dependencies = {
            'openvswitch': (DepType.USE, ['pull']),
            'neutron': (DepType.PROVIDE, ['deployed'])
        }

        self.initial_place = "initiated"

    def pull(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Pulling Neutron start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=pull"])

    def config(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Configuring Neutron start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_config"])

    def register(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Registering Neutron start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_register"])

    def deploy(self):
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Deploying Neutron start", host, ip)
        return run_ansible(self.playbook, self.inventory, ["action=mad_deploy"])

