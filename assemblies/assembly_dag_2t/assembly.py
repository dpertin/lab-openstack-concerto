from concerto.assembly import Assembly
from concerto.reconfiguration import Reconfiguration

from assemblies.assembly_dag_2t.facts import Facts
from assemblies.assembly_dag_2t.memcached import MemCached
from assemblies.assembly_dag_2t.common import Common
from assemblies.assembly_dag_2t.rabbitmq import RabbitMQ
from assemblies.assembly_dag_2t.openvswitch import OpenVSwitch
from assemblies.assembly_dag_2t.haproxy import HAProxy
from assemblies.assembly_dag_2t.mariadb import MariaDB
from assemblies.assembly_dag_2t.keystone import Keystone
from assemblies.assembly_dag_2t.glance import Glance
from assemblies.assembly_dag_2t.neutron import Neutron
from assemblies.assembly_dag_2t.nova import Nova


class Dag2tAssembly(Assembly):
    def deploy(self):
        self.set_print_time(True)
        self.run_reconfiguration(self.get_deploy_reconf())
    
    @staticmethod
    def get_deploy_reconf():
        reconf = Reconfiguration()
        
        # adding the components
        reconf.add("facts", Facts)
        reconf.add("common", Common)
        reconf.add("haproxy", HAProxy)
        reconf.add("memcached", MemCached)
        reconf.add("mariadb", MariaDB)
        reconf.add("rabbitmq", RabbitMQ)
        reconf.add("keystone", Keystone)
        reconf.add("openvswitch", OpenVSwitch)
        reconf.add("glance", Glance)
        reconf.add("neutron", Neutron)
        reconf.add("nova", Nova)

        # connecting components
        reconf.connect("facts", "facts", "haproxy", "facts")
        reconf.connect("facts", "facts", "openvswitch", "facts")
        reconf.connect("facts", "facts", "memcached", "facts")
        reconf.connect("facts", "facts", "rabbitmq", "facts")
        reconf.connect("facts", "facts", "common", "facts")

        reconf.connect("common", "common", "mariadb", "common")

        reconf.connect("haproxy", "haproxy", "mariadb", "haproxy")

        reconf.connect("mariadb", "mariadb", "keystone", "mariadb")

        reconf.connect("keystone", "keystone", "glance", "keystone")
        reconf.connect("keystone", "keystone", "nova", "keystone")
        reconf.connect("keystone", "keystone", "neutron", "keystone")

        # adding behaviours to the components
        reconf.push_behavior("facts", "deploy")
        reconf.push_behavior("memcached", "deploy")
        reconf.push_behavior("rabbitmq", "deploy")
        reconf.push_behavior("openvswitch", "deploy")
        reconf.push_behavior("common", "deploy")
        reconf.push_behavior("haproxy", "deploy")
        reconf.push_behavior("mariadb", "deploy")
        reconf.push_behavior("keystone", "deploy")
        reconf.push_behavior("glance", "deploy")
        reconf.push_behavior("neutron", "deploy")
        reconf.push_behavior("nova", "deploy")

        # doing things
        reconf.wait_all()
        
        return reconf
