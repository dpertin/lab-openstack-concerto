#!/usr/bin/env python3

import logging

import sys
import click
from subprocess import run

from assemblies.assembly_seq_1t.assembly import Seq1tAssembly
from assemblies.assembly_dag_2t.assembly import Dag2tAssembly
from assemblies.assembly_dag_nt4.assembly import DagNT4Assembly
from assemblies.assembly_seq_nt4.assembly import SeqNT4Assembly

from utils.extra import get_host_name_and_ip

timeout = 2000  # Set the maximum time before timeout
assemblies = ['seq_1t', 'dag_2t', 'dag_nt4', 'seq_nt4']


@click.command(help="Launch the assembly")
@click.option("-a", "--assembly_type",
              default="dag_nt4",
              type=click.Choice(assemblies),
              help="Select the assembly type.")
@click.option("-g", "--gantt",
              is_flag=True,
              help="Give a name for generating Gantt graphs.")
@click.option("-gn", "--gantt_name",
              default="server",
              help="Give a name for generating Gantt graphs.")
def main(assembly_type, gantt, gantt_name):

    logging.basicConfig(filename='concerto.log',
                        format='%(threadName)s - %(processName)s - %(asctime)s - %(filename)s - %(funcName)s - %(message)s',
                        level=logging.DEBUG)

    the_log = logging.getLogger("testing")

    logging.debug("Testing")
    assembly = None
    choice_assembly = assembly_type
    if choice_assembly == "seq_1t":
        assembly = Seq1tAssembly()
    elif choice_assembly == "dag_2t":
        assembly = Dag2tAssembly()
    elif choice_assembly == "dag_nt4":
        assembly = DagNT4Assembly()
    elif choice_assembly == "seq_nt4":
        assembly = SeqNT4Assembly()
    assembly.set_verbosity(1)
    if gantt:
        assembly.set_record_gantt(True)
    assembly.deploy()

    # Set a timeout regarding our deployment
    finished, debug_info = assembly.synchronize_timeout(timeout)

    if finished and not assembly.get_error_reports():# and not assembly.get_error_reports():
        print("Deployement successful")
        host, ip = get_host_name_and_ip()
        logging.debug("host: %s - ip: %s : Open Stack Assembly Done", host, ip)
        assembly.terminate()

        # The following is used to generate Gantt charts
        if gantt:
            file_name = "results_" + gantt_name
            gc = assembly.get_gantt_record().get_gantt_chart()
            gc.export_json(file_name + ".json")

            sys.exit(0)

    if assembly.get_error_reports():
        print("Deployement failed")
        assembly.kill()

    if not finished:
        print("Deployement failed")
        print("Error! Timeout on deploy! Debug info:")
        print(debug_info)
        assembly.kill()
        # Sometimes ansible-playbook is stuck, here's an ugly workaround:
        run(["pkill -f anisble-playbook"])

    sys.exit(1)


if __name__ == '__main__':
    main()

