#!/usr/bin/env python3

# Execo jobs can be tagged as 'todo', 'skipped', 'done', or 'cancel'. Most of
# the times, failing experiments are due to G5K (i.e. a node is temporary
# unavailable). As a consequence, we retry failing tests.

import os, sys, yaml
import time as timer
from pprint import pformat
from subprocess import run

from execo import *
from execo_g5k import *
from execo_engine import *



class MadBench(Engine):
    """An Execo engine to bench OpenStack deployment with MAD."""

    def __init__(self):
        self.registry = None
        self.mad = os.path.abspath(os.getcwd() + "/mad.py")
        self.assembly = os.path.abspath(os.getcwd() + "/assembly.py")

        super(MadBench, self).__init__()
        # Here we define the program's arguments:
        self.args_parser.add_argument(
                '-f', '--configuration-file',
                dest='config_file',
                default='reservation.yaml',
                help='Set the benchmark configuration file.')
        self.args_parser.add_argument(
                '-p', '--provider',
                dest='provider',
                help='Set the benchmark configuration file.')
        self.args_parser.add_argument(
                '--registry',
                dest='registry',
                choices=['cached', 'local', 'remote'],
                help='Set the registry mode.')
        self.args_parser.add_argument(
                '-t', '--test',
                dest='test',
                help='Set the test defined in the benchmark config file.')

    def run_mad(self, cmd):
        return run(["python3", self.mad] + cmd.split()).returncode

    def run_assembly(self, cmd):
        return run(["python3", self.assembly] + cmd.split()).returncode

    def create_paramsweeper(self):
        # A test must be provided if a configuration file is given:
        if self.args.config_file and self.args.test is None:
            self.args_parser.error(
                    "'-f|--benchmark-file' requires '-t|--test'.")

        # Set the benchmark parameters given the following priorities:
        # 1. params given as args
        # 2. params given from benchmark definition file

        if self.args.config_file:
            # Else, fetch the params from the benchmark definition file.
            # Check if the configuration file exists:
            if not os.path.isfile(self.args.config_file):
                print("The configuration file: " + self.args.config_file +
                        " does not exist - leaving the program :(")
                sys.exit(1)

            with open(self.args.config_file, "r") as f:
                test_definitions = yaml.load(f)
            
            # Check if the desired benchmark is defined in the definition file:
            if self.args.test not in test_definitions:
                print("The following test: " + self.args.test + " is not "
                        "defined in the given benchmark definition file: "
                        + self.args.config_file + " - leaving the program :(")
                sys.exit(1)

            test_definition = test_definitions[self.args.test]

            params = test_definition['params']

        self.registry = self.args.registry or \
                test_definition["params"].get("registry")
        if not self.registry:
            print("Warning, no registry configuration found - set as 'cached'")

        iterations = test_definition.get("iterations")
        if isinstance(iterations, int):
            iterations = range(0, test_definition.get("iterations"))
        params['repeat'] = tuple(iterations)

        logger.info('Defining parameters: %s', pformat(params))

        combs = sweep(params)
        sweeper = ParamSweeper(self.result_dir + "/sweeper", combs)

        return sweeper

    def filtr(self, combs):
        """Filter combs on a per-registry basis (set local first)."""
        return sorted(
                # Sort on a per-repeat basis first:
                sorted(combs, key=lambda r: r['repeat']),
                # Sort the result per registry:
                key=lambda t: 1 if t["registry"] == "local"
                    else ( 2 if t["registry"] == "cached"
                    else 3)
                )

    def redeploy(self, registry):
        """
        Redeploy is called for each new registry test. It is meant to deploy a new
        topology (e.g. when registry is 'local' an extra node is required).
        """

        # Each time we run the script, we bootstrap the deployment env:
        #   1. Be sure the infrastructure is up;
        #   2. Destroy previously downloaded docker images;
        #   3. Download every Docker images.

        # Destroy exising containers and images on every node:
        self.run_mad("destroy --include-images")

        # Update the EnOS configuration file according to the registry config:
        options = ""
        if registry == "local":
            options += " --registry"
        if self.args.provider:
            options += " -p " + self.args.provider
        options += " --tags registry"
        failure = self.run_mad("redeploy" + options)
        if failure:
            print("An issue happened: the infra is not up "
                    "- leave the program :(")
            sys.exit(1)

        # If registry is 'cached' or 'local', images must be downloaded:
        if registry == 'cached':
            self.run_mad("bootstrap --cached")
        elif registry == 'local':
            self.run_mad("bootstrap")

    def run(self):
        sweeper = self.create_paramsweeper()
        current_registry = None

        # Bench each combination. As mentioned, we observe sometimes temporary
        # failures due to G5K, thus, we reschedule failed iterations:
        while len(sweeper.get_remaining()) > 0:

            # Combinations are sorted regarding the registry configuration:
            comb = sweeper.get_next(filtr=self.filtr)
            logger.info('Treating combination %s', pformat(comb))

            if current_registry != comb["registry"]:
                # When a new type of registry is tested, redeployment is needed
                current_registry = comb["registry"]
                self.redeploy(comb["registry"])
            else:
                options=""
                # If registry is cached: only destroy containers
                if comb['registry'] == "cached":
                    options+=""
                # If registry is local: destroy containers + images on OS hosts
                elif comb['registry'] == "local":
                    self.run_mad("destroy")
                    opt = " --registry"
                    opt += " --tags registry"
                    self.run_mad("redeploy" + opt)
                    options+="--local"
                # If registry is remote: destroy containers + images on every hosts
                elif comb['registry'] == "remote":
                    options+="--include-images"
                self.run_mad("destroy " + options)

            gantt_name = comb['registry'] + "_" + comb['test_type'] + "_" + str(comb['repeat'])
            cmd = "--assembly_type " + comb['test_type']
            cmd += " --gantt"
            cmd += " --gantt_name " + gantt_name

            start = timer.perf_counter()
            failure = self.run_assembly(cmd)
            time = timer.perf_counter() - start
            
            prefix = ""
            if failure:
                # Archive the results in specific file and retry later the expe
                prefix = "fail_"
                sweeper.skip(comb)
            else:
                sweeper.done(comb)

            result_file = self.result_dir + "/results"
            results = { "params": comb, "time": time, "failure": failure }
            with open(result_file, "a") as f:
                yaml.dump([results], f, width = 72)

            logger.info('%s/%s', sweeper.get_done(), sweeper.get_sweeps())

            # Forget the skipped combs and replace them as remaining:
            sweeper.reset()


if __name__ == '__main__':
    e = MadBench()
    e.start()
