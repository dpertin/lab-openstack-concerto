
import math
import yaml
import jinja2
import os, sys
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from utils.constants import (ASSEMBLIES, SOLUTIONS, TEMPLATE_DIR)
from concerto.meta import ReconfigurationPerfAnalyzer

from execo import *
from execo_g5k import *
from execo_engine import *



def mean (list):
    if len(list) != 0:
        return sum(list)/len(list)
    else:
        return 0


def average_gantt_times(filenames):
    """
    :param filenames: iterable of names JSON Gantt charts to load
    :return: a dictionary mapping a tuple (component,transition) to the average duration of the transition in the Gantt
    charts
    """
    from statistics import mean
    times_list = []
    for fn in filenames:
        times_list.append(load_gantt_times(fn))
    times = dict()
    if not filenames:
        return times
    for key in times_list[0]:
        times[key] = mean([inst[key] for inst in times_list])
    return times


def minimum_gantt_times(filenames):
    """
    :param filenames: iterable of names JSON Gantt charts to load
    :return: a dictionary mapping a tuple (component,transition) to the average duration of the transition in the Gantt
    charts
    """
    times_list = []
    for fn in filenames:
        times_list.append(load_gantt_times(fn))
    times = dict()
    if not filenames:
        return times
    for key in times_list[0]:
        times[key] = min([inst[key] for inst in times_list])
    return times


def maximum_gantt_times(filenames):
    """
    :param filenames: iterable of names JSON Gantt charts to load
    :return: a dictionary mapping a tuple (component,transition) to the average duration of the transition in the Gantt
    charts
    """
    from statistics import mean
    times_list = []
    for fn in filenames:
        times_list.append(load_gantt_times(fn))
    times = dict()
    if not filenames:
        return times
    for key in times_list[0]:
        times[key] = max([inst[key] for inst in times_list])
    return times


def load_gantt_times(filename):
    """
    :param filename: name of the JSON Gantt chart to load
    :return: a dictionary mapping a tuple (component,transition) to the duration of the transition in the Gantt chart
    """
    from json import load
    with open(filename) as f:
        gantt = load(f)
    times = dict()
    for component_name, behaviors in gantt.items():
        for behavior, transitions in behaviors.items():
            assert(behavior == "deploy")
            for transition in transitions:
                transition_name = transition["name"]
                transition_duration = transition["end"]-transition["start"]
                times[(component_name, transition_name)] = transition_duration
    
    return times


def format_results(results):
    formatted_results = {
        assembly:
            { registry:
                [ result["time"]
                    for result in results
                    if (result["params"]["registry"] == registry
                        and result["params"]["test_type"] == assembly
                            and result["failure"] == 0) ]
              for registry in
              [ result["params"]["registry"] for result in results ] }
        for assembly in
        [ result["params"]["test_type"] for result in results ] }

    # Extract assemblies and registries from the result file:
    assemblies = tuple(
            sorted(
                formatted_results.keys(),
                key=lambda t: 1 if t == "seq_1t"
                    else (2 if t == "seq_nt4"
                    else (3 if t == "dag_2t"
                    else 4))))

    registries = tuple(
            sorted(
                formatted_results[assemblies[0]].keys(),
                key=lambda r: 1 if r == "remote"
                    else (2 if r == "local"
                    else 3)))

    num_iterations = 0
    for registry in registries:
        for assembly, tres in formatted_results.items():
            num_iterations = len(tres[registry])

    return (formatted_results, assemblies, registries, num_iterations)


def compute_means_stds_maxs(assemblies, registries, theoretic_times):
    formatted_means = {}
    formatted_stds = {}

    # Compute means and standard deviations:
    # TODO: check if all tests have same iteration number or warning + count it
    for registry in registries:
        formatted_means[registry] = {}
        formatted_stds[registry] = {}
        for assembly in assemblies:
            tres = theoretic_times[assembly]
            formatted_means[registry][assembly] = mean(tres[registry])
            print(assembly, registry, mean(tres[registry]))
            print(tres[registry])
            formatted_stds[registry][assembly] = np.std(tres[registry])

    # Compute max values:
    max_values = {}
    for registry in registries:
        max_values[registry] = max(formatted_means[registry].values())

    # Populate appropriate structures for matplotlib:
    means={}
    stds={}
    for registry in registries:
        means[registry]=[]
        stds[registry]=[]
        for assembly in assemblies:
            means[registry].append(formatted_means[registry][assembly])
            stds[registry].append(formatted_stds[registry][assembly])

    return (means, stds, max_values)


def generate_theoretic_dots(assemblies, experiment_reconfigurations, charts_dir):
    # TODO keep charts_dir in main code
    for exp, exp_info in experiment_reconfigurations.items():
        if exp in assemblies:
            reconf = exp_info['reconf']
            perf_analyzer = ReconfigurationPerfAnalyzer(reconf)
            graph = perf_analyzer.get_graph()
            graph.save_as_dot("%s/graph_%s.dot" % (charts_dir, exp))
            formula = perf_analyzer.get_exec_time_formula()
            with open("%s/formula_%s.txt" % (charts_dir, exp), "w") as f:
                f.write(formula.to_string(lambda x: '.'.join(x)))


def extract_measured_times(assemblies, registries, num_iterations,
        experiment_reconfigurations, results_dir):
    measured_times = {}
    for assembly, exp_info in experiment_reconfigurations.items():
        if assembly in assemblies:
            time_files_pattern = exp_info.get('time_files_pattern', None)
            avg_times = {}
            min_times = {}
            max_times = {}
            for registry in registries:
                avg_times.update({registry:
                    average_gantt_times(["%s/results_%s_%s_%d.json" %
                        (results_dir, registry, time_files_pattern, i) for i in
                        range(num_iterations)])})
                min_times.update({registry:
                    minimum_gantt_times(["%s/results_%s_%s_%d.json" %
                        (results_dir, registry, time_files_pattern, i) for i in
                        range(num_iterations)])})
                max_times.update({registry:
                    maximum_gantt_times(["%s/results_%s_%s_%d.json" %
                        (results_dir, registry, time_files_pattern, i) for i in
                        range(num_iterations)])})

            measured_times.update({
                assembly: {
                    "average": avg_times,
                    "minimum": min_times,
                    "maximum": max_times}
            })

    return measured_times


def compute_theoretic_times(assemblies, registries, experiment_reconfigurations,
        measured_times, charts_dir):
    theoretic_times = {}
    for exp, exp_info in experiment_reconfigurations.items():
        if exp in assemblies:
            theoretic_times[exp] = {}
            print("%s:" % exp)

            reconf = exp_info['reconf']
            perf_analyzer = ReconfigurationPerfAnalyzer(reconf)
            formula = perf_analyzer.get_exec_time_formula()

            for stat, time in measured_times[exp].items():
                print("- %s:" % stat)
                theoretic_times[exp][stat] = {}
                for scenario, stimes in time.items():
                    # Checking that we obtain the same time with graph traversal and formula evaluation
                    gt = perf_analyzer.get_exec_time(stimes)
                    theoretic_times[exp][stat][scenario] = gt
                    f = formula.evaluate(stimes)
                    if not math.isclose(gt, f, rel_tol=1e-10):
                        print("WARNING: Graph traversal time different than formula time!")
                        print("- graph traversal   : %f seconds" % perf_analyzer.get_exec_time(stimes))
                        print("- formula evaluation: %f seconds" % formula.evaluate(stimes))

                    print("  - %s : %f" % (scenario, gt))

                    title = "%s_%s_theo" % (exp, scenario)
                    gc = perf_analyzer.get_gantt_chart(stimes)
                    gc.export_json("%s/gantt_%s_%s.json" % (charts_dir, stat, title))
                    gc.export_plotly("%s/gantt_%s_%s.svg" % (charts_dir, stat, title), "%s" % title)

    formatted_theoretic_times = {
        stat:
            { registry:
                { assembly: theoretic_times[assembly][stat][registry]
                    for assembly in assemblies }
                for registry in registries }
            for stat in measured_times[assemblies[0]].keys()}

    return formatted_theoretic_times


def draw_deployment_charts(formatted_results, transition_results, assemblies,
        registries, means, stds, max_values):

    avgs = transition_results['average']
    mins = transition_results['minimum']
    maxs = transition_results['maximum']

    index = np.arange(len(assemblies))

    fig = plt.figure(tight_layout=True)
    gs = gridspec.GridSpec(2, len(assemblies))

    # This first subplot displays global commissioning times
    ax0 = fig.add_subplot(gs[0, :])

    bar_width = 0.25
    opacity = 0.6
    colors = ['b', 'r', 'g', 'm', 'c', 'y', 'k']

    fgains = { assembly: {
        registry: 0 for registry in registries }
        for assembly in assemblies }

    i=0
    avg_rects = {}
    for registry in registries:
        # Draw the average time measured from the benchmark as colored bars
        avg_rects[registry] = ax0.bar(x = index + i*bar_width,
                            height = means[registry], width = bar_width,
                            alpha=opacity, color=colors[i], label=registry)
        i=i+1
        j=0
        # Write the performance ratio over bars
        for rect in avg_rects[registry]:
            height = rect.get_height()
            if height == max_values[registry]:
                txt = 1.0
            else:
                txt = float(height/max_values[registry])
            # Add mean value on top of bars:
            ax0.text(rect.get_x() + rect.get_width()/2.0, height*1.02,
                    '%s' % int(means[registry][j]), color='black', ha='center', va='bottom')
            # Add mean ratio below top of bars:
            ax0.text(rect.get_x() + rect.get_width()/2.0, height*0.95,
                    '%.2f' % txt, color='white', ha='center', va='top')
            fgains[assemblies[j]][registry] = int(100-txt*100)
            j=j+1
    # Add extra space to display mean values on top of bars
    ax0.set_ylim(top=means['remote'][0] + 100)

    ax0.set_xlabel('Assemblies')
    ax0.set_ylabel('Time (s)')
    #ax0.set_title('Time to deploy OpenStack for different assemblies')
    ax0.set_xticks(index + bar_width)
    ax0.set_xticklabels(
            [x for x in SOLUTIONS if ASSEMBLIES[SOLUTIONS.index(x)] in assemblies])
    ax0.legend()

    # In the following, we build for each assembly a related subplot for a
    # better understanding of theoretic times and standard deviation.
    # First we transform the required data (means, stds, maxs, mins) into a more
    # appropriate format:
    fmeans = {
        assemblies[i]: {
            registry: int(means[registry][i])
            for registry in registries }
        for i in range(len(assemblies)) }
    fstds = {
        assemblies[i]: {
            registry: int(stds[registry][i])
            for registry in registries }
        for i in range(len(assemblies)) }
    favgs = {
        assembly: {
            registry: int(avgs[registry][assembly])
            for registry in registries }
        for assembly in assemblies }
    fmaxs = {
        assembly: {
            registry: int(maxs[registry][assembly])
            for registry in registries }
        for assembly in assemblies }
    fmins = {
        assembly: {
            registry: int(mins[registry][assembly])
            for registry in registries }
        for assembly in assemblies }

    # Few settings for graph display:
    bar_width = 1
    linewidth = 1
    edgecolor= 'k'

    # For each assembly, we add a new subplot displaying min-max as boxes, means
    # and standard deviation as error bars:
    for i in range(len(assemblies)):
        assembly = assemblies[i]
        ax = fig.add_subplot(gs[1, i])
        # Draw a box displaying min and max values from the performance model
        diff_max_min = [maxi-mini for maxi,mini in
                zip(fmaxs[assembly].values(), fmins[assembly].values())]
        ax.bar(x = index + i*bar_width,
            height = diff_max_min, width = bar_width,
            bottom = list(fmins[assembly].values()),
            linewidth=linewidth,
            facecolor="White", edgecolor=edgecolor)
        eb = ax.errorbar(x = index + i*bar_width,
            y = fmeans[assembly].values(),
            xerr=0.4, yerr=fstds[assembly].values(),
            ecolor=colors[:len(assemblies)],
            fmt='none')
        elines = eb.get_children()
        elines[1].set_color('k')
        ax.tick_params(
            axis='x',               # changes apply to the x-axis
            which='both',           # both major and minor ticks are affected
            bottom=False,           # ticks along the bottom edge are off
            top=False,              # ticks along the top edge are off
            labelbottom=False)      # labels along the bottom edge are off
        if i == 0:
            ax.set_ylabel('Time (s)')
        ax.grid(True, axis = 'y')
        ax.set_axisbelow(True)

    table_results = {
      assembly: {
        'measured mean (s)': {
          registry: fmeans[assembly][registry]
             for registry in registries },
        'measured gain (\%)': {
          registry: fgains[assembly][registry]
             for registry in registries },
        'measured std dvt (s)': {
          registry: fstds[assembly][registry]
             for registry in registries },
        'theoretical mean (s)': {
          registry: favgs[assembly][registry]
             for registry in registries },
        'theoretical max (s)': {
          registry: fmaxs[assembly][registry]
             for registry in registries },
        'theoretical min (s)': {
          registry: fmins[assembly][registry]
             for registry in registries }
        } for assembly in assemblies }

    table_results = {
        'measured': {
            'mean': {
                SOLUTIONS[ASSEMBLIES.index(assembly)]: {
                    registry: fmeans[assembly][registry]
                        for registry in registries }
                    for assembly in assemblies },
            'gain': {
                SOLUTIONS[ASSEMBLIES.index(assembly)]: {
                    registry: fgains[assembly][registry]
                        for registry in registries }
                    for assembly in assemblies },
            'std': {
                SOLUTIONS[ASSEMBLIES.index(assembly)]: {
                    registry: fstds[assembly][registry]
                        for registry in registries }
                    for assembly in assemblies }},
        'theoretical': {
            'max': {
                SOLUTIONS[ASSEMBLIES.index(assembly)]: {
                    registry: fmaxs[assembly][registry]
                        for registry in registries }
                    for assembly in assemblies },
#            'mean': {
#                SOLUTIONS[ASSEMBLIES.index(assembly)]: {
#                    registry: favgs[assembly][registry]
#                        for registry in registries }
#                    for assembly in assemblies },
            'min': {
                SOLUTIONS[ASSEMBLIES.index(assembly)]: {
                    registry: fmins[assembly][registry]
                        for registry in registries }
                    for assembly in assemblies }}}

    return (table_results, plt)


def generate_latex_table(assemblies, registries, table_results):
    latex_jinja_env = jinja2.Environment(
            block_start_string = '\BLOCK{',
            block_end_string = '}',
            variable_start_string = '\VAR{',
            variable_end_string = '}',
            comment_start_string = '\#{',
            comment_end_string = '}',
            line_statement_prefix = '%%',
            line_comment_prefix = '%#',
            trim_blocks = True,
            autoescape = False,
            loader = jinja2.FileSystemLoader(os.path.abspath('.'))
    )

    template = latex_jinja_env.get_template(TEMPLATE_DIR + '/latex_table2.tex')

    #num_criteria = len(table_results[assemblies[0]])

    renderer_template = template.render(
            assemblies = [x for x in SOLUTIONS if ASSEMBLIES[SOLUTIONS.index(x)]
                in assemblies],
            registries = registries,
            table_results = table_results)
            #num_criteria = num_criteria)

    print(renderer_template)

    return renderer_template

